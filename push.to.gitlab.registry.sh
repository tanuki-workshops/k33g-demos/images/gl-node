#!/bin/bash

set -o allexport; source .env; set +o allexport
echo "🐋 ${IMAGE_NAME}:${IMAGE_TAG}"

docker login registry.gitlab.com -u ${GITLAB_HANDLE} -p ${GITLAB_TOKEN_ADMIN}
docker tag ${IMAGE_NAME} registry.gitlab.com/${IMAGE_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}
docker push registry.gitlab.com/${IMAGE_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}

