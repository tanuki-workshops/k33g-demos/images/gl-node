#!/bin/bash

set -o allexport; source .env; set +o allexport
echo "🐋 ${IMAGE_NAME}:${IMAGE_TAG}"

mkdir tmp
cd tmp
wget https://gitlab.com/gitlab-org/cli/-/releases/v1.31.0/downloads/glab_1.31.0_Linux_x86_64.tar.gz
tar -xf glab_1.31.0_Linux_x86_64.tar.gz 
cd ..

docker build -t ${IMAGE_NAME} . 

rm -rf tmp
