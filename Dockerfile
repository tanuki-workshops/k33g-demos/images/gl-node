#FROM golang:latest as build-env
#FROM node:20-slim

FROM node:20-alpine AS node

FROM alpine:latest

RUN apk add --no-cache wget
#RUN apk add --no-cache node
RUN apk add git

COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

COPY tmp/bin/glab /usr/local/bin